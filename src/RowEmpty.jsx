import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { getRowClassNames } from './helpers';

export default function RowEmpty(props) {
  const { classes } = props;
  const classNames = getRowClassNames(props);
  return (
    <div className={cx(classNames)} role="row">
      <div
        className={classes.tdEmpty}
        children="No records found"
      />
    </div>
  );
}

RowEmpty.propTypes = {
  classes: PropTypes.shape({
    tr: PropTypes.string.isRequired,
    tdEmpty: PropTypes.string.isRequired,
  }).isRequired,
};

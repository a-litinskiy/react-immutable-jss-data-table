import React from 'react';
import PropTypes from 'prop-types';

export default function TBody({ classes, children }) {
  return <div className={classes.tbody} children={children} />;
}

TBody.propTypes = {
  classes: PropTypes.shape({
    tbody: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.node,
};

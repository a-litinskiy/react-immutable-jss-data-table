import React from 'react';
import PropTypes from 'prop-types';
import renderer from 'react-test-renderer';
import I from 'immutable';
import { setPropTypes } from '../helpers';

import { NoDataExtractor } from '../helpers';
import Cell from '../Cell';

describe('Cell', () => {
  const rowData = I.Map({ title: 'my title' });
  const fieldPath = ['title'];

  it('renders own markup if other not declared', () => {
    const columnDef = {};
    const component = renderer.create((
      <Cell
        classes={{ td: 'td' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('cellClasses[className] || *classes[className] || className', () => {
    const columnDef = {
      className: 'classNameKey',
    };
    const component = renderer.create((
      <Cell
        classes={{ td: 'td-v', classNameKey: 'classes-className' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('*cellClasses[className] || classes[className] || className', () => {
    const columnDef = {
      className: 'classNameKey',
    };
    const component = renderer.create((
      <Cell
        classes={{ td: 'td-v', classNameKey: 'classes-className' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
        cellClasses={{ classNameKey: 'cellClasses-className' }}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('cellClasses[className] || classes[className] || *className', () => {
    const columnDef = {
      className: 'classNameKey',
    };
    const component = renderer.create((
      <Cell
        classes={{ td: 'td-v' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('uses getValue to show content', () => {
    const getValue = setPropTypes(
      ({ value }) => <h1 children={value} />,
      {
        value: PropTypes.node,
      },
      'custom.getValue',
    );
    const columnDef = {};
    const component = renderer.create((
      <Cell
        classes={{ td: 'td-v' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
        getValue={getValue}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('uses definded render function as component', () => {
    const render = setPropTypes(
      ({ value, className }) => <li className={className} >title is {value}</li>,
      {
        value: PropTypes.node.isRequired,
        className: PropTypes.string.isRequired,
      },
      'CustomCellRenderer',
    );
    const columnDef = {
      className: 'extraClass',
    };
    const component = renderer.create((
      <Cell
        classes={{ td: 'td', extraClass: 'extraClass-v' }}
        rowData={rowData}
        rowIndex={0}
        columnDef={columnDef}
        fieldPath={fieldPath}
        value={rowData.getIn(fieldPath)}
        render={render}
      />
    ));
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('throws NoDataExtractor if no (`name` or `fieldPath` or `render` or `getValue`) is specified', () => {
    const columnDef = {
      render: undefined,
      fieldPath: undefined,
      getValue: undefined,
    };
    const render = () => {
      renderer.create((
        <Cell
          classes={{ td: 'td', extraClass: 'extraClass-v' }}
          rowData={rowData}
          rowIndex={0}
          columnDef={columnDef}
          render={null}
        />
      ));
    };
    expect(render).toThrow(NoDataExtractor);
  });

});

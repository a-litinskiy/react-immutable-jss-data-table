import { mount } from 'enzyme';
import DataTableDefault, {
  // default getters
  renderTh,
  getValueTh,
  getValue,
  renderCell,
  // components
  Th,
  TBody,
  THead,
  Row,
  RowEmpty,
  Cell,
  DataTable,
  // sheet example
  sheet,
  // helpers
  getCellClassNames,
  getThClassNames,
  getRowClassNames,
  getColumnDef,
  columnsSelector,
} from '../index';


describe('index.js', () => {
  it('getValueTh', () => {
    expect(getValueTh).toBeDefined();
  });
  it('getValue', () => {
    expect(getValue).toBeDefined();
  });
  it('renderCell', () => {
    expect(renderCell).toBeDefined();
  });
  it('Th', () => {
    expect(Th).toBeDefined();
  });
  it('TBody', () => {
    expect(TBody).toBeDefined();
  });
  it('THead', () => {
    expect(THead).toBeDefined();
  });
  it('Row', () => {
    expect(Row).toBeDefined();
  });
  it('RowEmpty', () => {
    expect(RowEmpty).toBeDefined();
  });
  it('Cell', () => {
    expect(Cell).toBeDefined();
  });
  it('DataTable', () => {
    expect(DataTable).toBeDefined();
  });
  it('sheet', () => {
    expect(sheet).toBeDefined();
  });
  it('getCellClassNames', () => {
    expect(getCellClassNames).toBeDefined();
  });
  it('getThClassNames', () => {
    expect(getThClassNames).toBeDefined();
  });
  it('getRowClassNames', () => {
    expect(getRowClassNames).toBeDefined();
  });
  it('getColumnDef', () => {
    expect(getColumnDef).toBeDefined();
  });
  it('columnsSelector', () => {
    expect(columnsSelector).toBeDefined();
  });
});

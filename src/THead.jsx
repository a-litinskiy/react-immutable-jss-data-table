import React from 'react';
import PropTypes from 'prop-types';

export default function THead({ classes, children }) {
  return <div className={classes.thead} children={children} />;
}

THead.propTypes = {
  classes: PropTypes.shape({
    thead: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.node,
};

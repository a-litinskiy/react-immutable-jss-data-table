import React from 'react';
import PropTypes from 'prop-types';
import I from 'immutable';
import cx from 'classnames';
// components
import Th from './Th';
import Row from './Row';
import RowEmpty from './RowEmpty';
import { getRowClassNames, getCellClassNames, getThClassNames, isValidReactComponent } from './helpers';
import Cell from './Cell';
import TBody from './TBody';
import THead from './THead';

class DataTable extends React.PureComponent {
  static propTypes = {
    columns: PropTypes.array.isRequired,
    items: PropTypes.instanceOf(I.List).isRequired,
    classes: PropTypes.shape({
      DataTable: PropTypes.string.isRequired,
      thead: PropTypes.string,
      tbody: PropTypes.string.isRequired,
      tr: PropTypes.string.isRequired,
      trEven: PropTypes.string,
      trOdd: PropTypes.string,
      trh: PropTypes.string,
      th: PropTypes.string.isRequired,
      td: PropTypes.string.isRequired,
      tdEmpty: PropTypes.string,
    }).isRequired,
    getRowClassNames: PropTypes.func,
    getCellClassNames: PropTypes.func,
    getThClassNames: PropTypes.func,
    showColumnsTitles: PropTypes.bool,
    className: PropTypes.string,
    Row: isValidReactComponent,
    Cell: isValidReactComponent,
    THead: isValidReactComponent,
    TBody: isValidReactComponent,
    Th: isValidReactComponent,
    RowEmpty: isValidReactComponent,
    renderTh: PropTypes.func,
    getValueTh: PropTypes.func,
    getValue: PropTypes.func,
    render: PropTypes.func,
  };

  static defaultProps = {
    showColumnsTitles: false,
    className: '',
    getRowClassNames,
    Cell,
    Row,
    THead,
    TBody,
    Th,
    getCellClassNames,
    getThClassNames,
    RowEmpty,
  };

  renderRow = (rowData, rowIndex) => {
    const { items, Row } = this.props;
    return <Row
      key={rowIndex}
      {...{
        ...this.props,
        rowIndex,
        rowData,
        rowsCount: items.size,
      }}
    />;
  }

  render() {
    const { columns, items, classes, showColumnsTitles, className, THead, TBody, Th, RowEmpty, getRowClassNames } = this.props;
    return (
      <div className={cx(classes.DataTable, className)}>
        {
          showColumnsTitles &&
            <THead {...this.props}>
              <div className={cx(getRowClassNames(this.props), classes.trh)}>
                {
                  columns.map((columnDef, key) => (
                    <Th
                      key={key}
                      {...{ ...this.props, columnDef }}
                      title={columnDef.title || ''}
                      getValueTh={columnDef.getValueTh || this.props.getValueTh}
                      renderTh={columnDef.renderTh || this.props.renderTh}
                    />
                  ))
                }
              </div>
            </THead>
        }
        <TBody {...this.props}>
          {
            items.isEmpty() ?
              <RowEmpty {...this.props} />
              :
              items.map(this.renderRow)
          }
        </TBody>
      </div>
    );
  }
}

export default DataTable;
